pub trait App<Backend: ratatui::backend::Backend> {
    fn render(&mut self, terminal: &mut ratatui::Terminal<Backend>);
    fn handle_event(&mut self);
}
