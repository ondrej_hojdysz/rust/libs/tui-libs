use crossterm::{
    event::{self},
    terminal::{disable_raw_mode, enable_raw_mode, EnterAlternateScreen, LeaveAlternateScreen},
    ExecutableCommand,
};
use ratatui::prelude::{CrosstermBackend, Terminal};

use crossterm::event::{KeyCode, KeyEvent, KeyEventKind};
use nucleo_matcher::{pattern::Pattern, Config, Matcher};
use ratatui::{
    prelude::{Constraint, Layout, Rect},
    style::{Color, Style},
    text::Text,
    widgets::{Block, Borders, Paragraph},
    Frame,
};
use std::io::{stdout, Result};

pub struct FuzzySelectDialog {
    searchstr: String,
    items: Vec<String>,
    matches: String,
    index: usize,
    matchcount: usize,
    selected: String,
}

pub enum DialogResult {
    Close,
    Value(String),
}

impl FuzzySelectDialog {
    pub fn new(items: Vec<String>) -> Self {
        let mut config = Config::DEFAULT.clone();
        config.ignore_case = true;
        let mut me = Self {
            items,
            searchstr: String::new(),
            matches: String::new(),
            selected: String::new(),
            index: 0,
            matchcount: 0,
        };

        me.update_matches();
        me
    }

    fn matcher_config() -> Config {
        let mut config = Config::DEFAULT.clone();
        config.ignore_case = true;

        config
    }

    pub fn render(&self, area: Rect, frame: &mut Frame) {
        let chunks = Layout::default()
            .direction(ratatui::prelude::Direction::Vertical)
            .constraints([Constraint::Length(3), Constraint::Min(1)])
            .split(area);

        self.render_search(chunks[0], frame);
        self.render_matches(chunks[1], frame);
    }

    pub fn key_press(&mut self, keyevent: &KeyEvent) -> Option<DialogResult> {
        if keyevent.kind == KeyEventKind::Press {
            match keyevent.code {
                KeyCode::Backspace => {
                    self.searchstr.pop();
                    self.update_matches();
                    None
                }
                KeyCode::Char(value) => {
                    self.searchstr.push(value);
                    self.update_matches();
                    None
                }
                KeyCode::Up => {
                    self.decrement_index();
                    None
                }
                KeyCode::Down => {
                    self.increment_index();
                    None
                }
                KeyCode::Enter => {
                    let mut output = String::new();
                    std::mem::swap(&mut output, &mut self.selected);
                    Some(DialogResult::Value(output))
                }
                KeyCode::Esc => Some(DialogResult::Close),

                _ => None,
            }
        } else {
            None
        }
    }

    fn render_search(&self, area: Rect, frame: &mut Frame) {
        let title_block = Block::default()
            .borders(Borders::ALL)
            .style(Style::default());

        let title = Paragraph::new(Text::styled(
            &self.searchstr,
            ratatui::style::Style::default().fg(Color::Green),
        ))
        .block(title_block);

        frame.render_widget(title, area);
    }

    fn render_matches(&self, area: Rect, frame: &mut Frame) {
        let title_block = Block::default()
            .borders(Borders::ALL)
            .style(Style::default());

        let title = Paragraph::new(Text::styled(
            &self.matches,
            ratatui::style::Style::default().fg(Color::Green),
        ))
        .block(title_block);

        frame.render_widget(title, area);
    }

    fn update_matches(&mut self) {
        let mut matcher = Matcher::new(Self::matcher_config());
        let mut output = String::new();
        self.clamp_index();
        self.matchcount = 0;
        Pattern::parse(
            &self.searchstr,
            nucleo_matcher::pattern::CaseMatching::Smart,
        )
        .match_list(&self.items, &mut matcher)
        .into_iter()
        .enumerate()
        .for_each(|(index, (value, _))| {
            output += if self.index == index {
                self.selected = value.clone();
                "* "
            } else {
                "  "
            };
            output += value;
            output += "\n";
            self.matchcount += 1;
        });

        self.matches = output
    }

    fn increment_index(&mut self) {
        if self.matchcount == 0 {
            self.index = 0;
        } else {
            self.index += 1;
            if self.index >= self.matchcount {
                self.index = 0;
            }
            self.update_matches()
        }
    }

    fn decrement_index(&mut self) {
        if self.matchcount == 0 {
            self.index = 0;
        } else {
            if self.index == 0 {
                self.index = self.matchcount - 1;
            } else {
                self.index -= 1;
            }
            self.update_matches()
        }
    }

    fn clamp_index(&mut self) {
        if self.matchcount == 0 {
            self.index = 0;
        } else {
            self.index = self.index.clamp(0, self.matchcount - 1);
        }
    }
}
pub fn run_fuzzy_dialog(items: Vec<String>) -> Result<Option<String>> {
    stdout().execute(EnterAlternateScreen)?;
    enable_raw_mode()?;
    let mut terminal = Terminal::new(CrosstermBackend::new(stdout()))?;
    terminal.clear()?;
    let mut output = None;
    let mut searcher = FuzzySelectDialog::new(items);
    loop {
        terminal.draw(|frame| {
            let area = frame.size();
            searcher.render(area, frame);
        })?;

        if event::poll(std::time::Duration::from_millis(16))? {
            if let event::Event::Key(key) = event::read()? {
                if let Some(event) = searcher.key_press(&key) {
                    if let DialogResult::Value(value) = event {
                        output = Some(value);
                    }

                    break;
                }
            }
        }
    }
    stdout().execute(LeaveAlternateScreen)?;
    disable_raw_mode()?;
    Ok(output)
}
