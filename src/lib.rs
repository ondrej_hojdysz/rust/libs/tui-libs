pub mod app_trait;
pub mod widgets;

// pub fn run_loop() -> Result<()> {
//     stdout().execute(EnterAlternateScreen)?;
//     enable_raw_mode()?;
//     let mut terminal = Terminal::new(CrosstermBackend::new(stdout()))?;
//     terminal.clear()?;
//     let mut output = String::new();
//     let mut searcher = widgets::fuzzy_select_dialog::FuzzySelectDialog::new(
//         ["bla".to_string(), "aeu".to_string(), "lcg".to_string()].to_vec(),
//     );
//     loop {
//         terminal.draw(|frame| {
//             let area = frame.size();
//             searcher.render(area, frame);
//         })?;

//         if event::poll(std::time::Duration::from_millis(16))? {
//             if let event::Event::Key(key) = event::read()? {
//                 if let Some(event) = searcher.key_press(&key) {
//                     match event {
//                         widgets::fuzzy_select_dialog::DialogResult::Value(value) => {
//                             output = value;
//                         }
//                         _ => {}
//                     }

//                     break;
//                 }
//             }
//         }
//     }
//     stdout().execute(LeaveAlternateScreen)?;
//     disable_raw_mode()?;
//     println!("Selected value: {output}");
//     Ok(())
// }

// #[cfg(test)]
// mod tests {
//     use super::*;
// }
